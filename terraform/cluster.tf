resource "yandex_kubernetes_cluster" kezan-wordpress {
 network_id = yandex_vpc_network.kezan-wordpress.id
 master {
   zonal {
     zone      = yandex_vpc_subnet.kezan-wordpress.zone
     subnet_id = yandex_vpc_subnet.kezan-wordpress.id
   }
 }
 service_account_id      = yandex_iam_service_account.kezan-wordpress.id
 node_service_account_id = yandex_iam_service_account.kezan-wordpress.id
   depends_on = [
     yandex_resourcemanager_folder_iam_binding.editor,
     yandex_resourcemanager_folder_iam_binding.images-puller
   ]
}

resource "yandex_vpc_network" "kezan-wordpress" { name = "kezan-wordpress" }

resource "yandex_vpc_subnet" "kezan-wordpress" {
 v4_cidr_blocks = ["10.100.1.0/24"]
 zone           = "${var.yandex_zone}"
 network_id     = yandex_vpc_network.kezan-wordpress.id
}

resource "yandex_iam_service_account" "kezan-wordpress" {
 name        = "kezan-wordpress"
 description = "Test SA"
}

resource "yandex_resourcemanager_folder_iam_binding" "editor" {
 # Service account to be assigned "editor" role.
 folder_id = "${var.yandex_folder_id}"
 role      = "editor"
 members   = [
   "serviceAccount:${yandex_iam_service_account.kezan-wordpress.id}"
 ]
}

resource "yandex_resourcemanager_folder_iam_binding" "images-puller" {
 # Service account to be assigned "container-registry.images.puller" role.
 folder_id = "${var.yandex_folder_id}"
 role      = "container-registry.images.puller"
 members   = [
   "serviceAccount:${yandex_iam_service_account.kezan-wordpress.id}"
 ]
}
